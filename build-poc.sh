#!/usr/bin/env bash
set -e

# Builds repositories for the OpenBMC and SatMC for the CPER PoC after setup.
# Author: Lawrence.Tang@arm.com
CYAN='\033[0;36m'
GREEN='\033[0;32m'
NC='\033[0m'
echo -e "${CYAN}Beginning build process...${NC}"
START_DIR=$PWD

# Build the BMC.
echo -e "\n${CYAN}Building OpenBMC...${NC}"
cd openbmc
. setup ast2600-a1
bitbake obmc-phosphor-image
cd $START_DIR

# Build the SatMC
echo -e "\n${CYAN}Building SatMC...${NC}"
cd zephyr/apps/SatMC
export PATH=~/.local/bin:$PATH
west build -b arduino_due

# Move out built files.
echo -e "\n${CYAN}Transferring build files...${NC}"
cd $START_DIR
mv openbmc/build/ast2600-a1/tmp/deploy/images/ast2600-a1/image-bmc ./image-bmc
mv zephyr/apps/SatMC/build/zephyr/zephyr.bin ./zephyr.bin

echo -e "${GREEN}Build complete.${NC}"