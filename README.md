# CPER-JSON PoC
This repository houses scripts and documentation for reproducing a CPER-JSON proof of concept utilising a SatMC board and OpenBMC board to communicate CPER messages, and save them as CPER-JSON through logging. This logging is then available through Redfish as well as through debug serial. The only boards tested with this PoC are the Arduino Due (SatMC) and ASPEED AST2600-A1 (OpenBMC).

These scripts have only been tested with Ubuntu 20.04 Focal (LTS). They should also work on Ubuntu 18.04 Bionic (LTS), however they will not on newer versions 22.04 and above (SatMC dependency `kitware-archive` does not support these versions).

## Building
First, clone this git repository, and then run the "setup-poc" and "build-poc" scripts. These should automatically install dependencies and then build the PoC output files for you.
```
git clone git@git.gitlab.arm.com:server_management/cper-poc-scripts.git cper-poc
cd cper-poc
./setup-poc.sh
./build-poc.sh
```

Once this is done, in the "cper-poc" directory you should have two files:

- `image-bmc`: This is the image that will be flashed onto the AST2600-A1, and contains the necessary OpenBMC software.
- `zephyr.bin`: This is the image that will be flashed onto the SatMC (tested on Arduino Due) to act as the SatMC.

These should be flashed onto your OpenBMC and SatMC boards respectively.

## Testing
First, you should wire your BMC to the SatMC as shown below:
![](cabling.png)

Once both boards are flashed and running, and the SatMC is correctly wired to the BMC, you can log into the OpenBMC terminal via. UART with user `root` and password `0penBmc`, and run the following to observe the detected sensors that PLDM can access:
```
busctl tree xyz.openbmc_project.PLDM 
```
If nothing appears, ensure that your SatMC is correctly connected to the BMC.

To trigger the CPER event, we can use `pldmtool` for platform data like so:
```
pldmtool platform getstatesensorreadings -m 8 -i 1 -r 0
```
This will trigger logging in the `/var/log/pldm` file, as well as in `/var/log/pldm.d`. You may have to run the command more than once to see `/var/log/pldm` appear. To read the logged CPER JSON output, run `pldmtool getplatformcperlogs`.

You can also access the logged data over the `bmcweb` Redfish API. To do so, you can use the following API endpoints:
```
export bmc=xx.xx.xx.xx
curl -k https://${bmc}/redfish/v1/Systems/system/LogServices/PldmEvent/Entries -u root:0penBmc # Find entry IDs here.
curl -k https://${bmc}/redfish/v1/Systems/system/LogServices/PldmEvent/Entries/XXXXX -u root:0penBmc # Find additional data IDs here.
curl -k https://${bmc}/redfish/v1/Systems/system/LogServices/PldmEvent/Entries/XXXXX/entryY_Z -u root:0penBmc # View additional data.
```