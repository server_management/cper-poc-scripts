#!/usr/bin/env bash
set -e

# Sets up dependencies & repositories for the OpenBMC and SatMC for the CPER PoC.
# Author: Lawrence.Tang@arm.com
CYAN='\033[0;36m'
GREEN='\033[0;32m'
NC='\033[0m'
echo -e "${CYAN}Beginning dependency install & setup for OpenBMC and SatMC build...${NC}"

# Clone the BMC.
START_DIR=$PWD
echo -e "\n${CYAN}Cloning OpenBMC...${NC}"
if [ ! -d "./openbmc" ] ; then
  git clone https://git.gitlab.arm.com/server_management/openbmc.git
fi

# Install BMC dependencies, build BMC.
echo -e "\n${CYAN}Installing BMC dependencies...${NC}"
sudo apt-get install -y git build-essential libsdl1.2-dev texinfo gawk chrpath diffstat zstd

# Ensure en_US.UTF8 is available for BMC build.
echo -e "\n${CYAN}Enabling en_US.UTF8 locale for BMC build...${NC}"
sudo locale-gen en_US.UTF-8
sudo update-locale 

# Install SatMC dependencies.
echo -e "\n${CYAN}Installing SatMC dependencies...${NC}"
wget https://apt.kitware.com/kitware-archive.sh
sudo bash kitware-archive.sh
rm kitware-archive.sh
sudo apt install --no-install-recommends git cmake ninja-build gperf \
  ccache dfu-util device-tree-compiler wget \
  python3-dev python3-pip python3-setuptools python3-tk python3-wheel xz-utils file \
  make gcc gcc-multilib g++-multilib libsdl2-dev
pip3 install --user -U west

# Initialise zephyr with west.
echo -e "\n${CYAN}Initialising Zephyr repository & dependencies...${NC}"
export PATH=~/.local/bin:$PATH
west init -m https://git.gitlab.arm.com/server_management/zephyr.git zephyr
cd zephyr
west update
west zephyr-export
pip3 install --user -r zephyr/scripts/requirements.txt

# Install toolchain if the user asks for it.
echo -e "\n${CYAN}Installing Zephyr toolchain (optional)...${NC}"
read -p "Install the Zephyr toolchain? (Do this if you have no toolchain installed.) Y/N: " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
  cd $START_DIR
  wget https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v0.13.0/zephyr-sdk-0.13.0-linux-x86_64-setup.run
  chmod +x zephyr-sdk-0.13.0-linux-x86_64-setup.run
  ./zephyr-sdk-0.13.0-linux-x86_64-setup.run
  rm ./zephyr-sdk-0.13.0-linux-x86_64-setup.run
fi

# Clone SatMC.
echo -e "${CYAN}Cloning SatMC repository...${NC}"
cd $START_DIR
cd zephyr
mkdir -p apps
cd apps
if [ ! -d "./satmc" ] ; then
  git clone https://git.gitlab.arm.com/server_management/satmc.git SatMC
fi

# Exit back out to home folder.
cd $START_DIR
echo -e "${GREEN}Setup complete! Run 'build-poc.sh' to build the PoC.${NC}"